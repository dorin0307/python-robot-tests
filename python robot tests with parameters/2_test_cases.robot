*** Settings ***
Documentation     A test suite containing 2 tests - the first one compares the passed parameter to 5, the other should be failed.
...				  if the parameter is 5 - the first test case should be passed.
...				  The suite should fail.

*** Variables ***
${num}			

*** Test Case ***
Should pass if passing 5
	compare    ${num}
Should fail
	compare  6

*** Keywords ***
compare
	[Arguments]    ${num}
    Should Be Equal 	${num} 	5
