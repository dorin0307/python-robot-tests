*** Settings ***
Documentation     A test suite containing one test that compares the passed parameter to 5.
...				  The suite should pass successfully if you pass 5.

*** Variables ***
${num}			

*** Test Case ***
Should pass if passing 5
	compare    ${num}

*** Keywords ***
compare
	[Arguments]    ${num}
    Should Be Equal 	${num} 	5
